window.onload = async () => {
  // Variables

  //Variable utilizada para mostrar el carrito al dar click en la imagen de ese, al inicia sera false
  var isActive = false;

  //Variable utilizada para guardar los productos traidos de la base de datos.
  var baseDeDatos;
  var baseDeDatosTotal;
  try {
    const resp = await fetch("https://bsale-backend-pe.herokuapp.com/product");
    baseDeDatos = await resp.json();
    baseDeDatosTotal = baseDeDatos;
  } catch (error) {}

  let carrito = [];
  let total = 0;
  const DOMitems = document.querySelector("#items");
  const DOMcarrito = document.querySelector("#carrito");
  const input = document.getElementById("buscador");
  const DOMtotal = document.querySelector("#total");
  const DOMbotonVaciar = document.querySelector("#boton-vaciar");
  const carritoIcon = document.getElementById("seeMarket");
  const rowCarrito = document.getElementById("row-carrito");

  rowCarrito.style.display = "none";
  carritoIcon.addEventListener("click", () => {
    isActive = !isActive;
    if (isActive) {
      rowCarrito.style.display = "block";
      return;
    }
    rowCarrito.style.display = "none";
  });

  // Funciones

  /**
   * Dibuja todos los productos a partir de la base de datos.
   * Pd: No confundir con el carrito
   */
  function renderizarProductos() {
    baseDeDatos.forEach((infoMaster) => {
      infoMaster.forEach((info) => {
        // Estructura
        const miNodo = document.createElement("div");
        miNodo.classList.add("card", "col-sm-4");
        // Body
        const miNodoCardBody = document.createElement("div");
        miNodoCardBody.classList.add("card-body");
        // Titulo
        const miNodoTitle = document.createElement("h5");
        miNodoTitle.classList.add("card-title");
        miNodoTitle.textContent = info.name;

        // Br
        const miNodoBr = document.createElement("hr");

        // Imagen
        const miNodoImagen = document.createElement("img");
        miNodoImagen.classList.add("img-fluid");
        miNodoImagen.setAttribute("src", info.url_image);
        // Precio
        const miNodoPrecio = document.createElement("p");
        miNodoPrecio.classList.add("card-text");
        miNodoPrecio.textContent = "$/. " + info.price;

        //Categoria
        const miNodoCategory = document.createElement("p");
        miNodoCategory.classList.add("card-text");
        miNodoCategory.textContent = "" + info.category.name;

        // Boton
        const miNodoBoton = document.createElement("button");
        miNodoBoton.classList.add("btn", "btn-secondary");
        miNodoBoton.textContent = "+";
        miNodoBoton.setAttribute("marcador", info.id);
        miNodoBoton.addEventListener("click", anyadirProductoAlCarrito);

        // Insertamos
        miNodoCardBody.appendChild(miNodoImagen);
        miNodoCardBody.appendChild(miNodoTitle);
        miNodoCardBody.appendChild(miNodoBr);
        miNodoCardBody.appendChild(miNodoCategory);
        miNodoCardBody.appendChild(miNodoPrecio);
        miNodoCardBody.appendChild(miNodoBoton);
        miNodo.appendChild(miNodoCardBody);
        DOMitems.appendChild(miNodo);
      });
    });
  }

  /**
   * Evento para añadir un producto al carrito de la compra
   */
  function anyadirProductoAlCarrito(evento) {
    // Anyadimos el Nodo a nuestro carrito
    carrito.push(evento.target.getAttribute("marcador"));
    // Calculo el total
    calcularTotal();
    // Actualizamos el carrito
    renderizarCarrito();
  }

  /**
   * Dibuja todos los productos guardados en el carrito
   */
  function renderizarCarrito() {
    // Vaciamos todo el html
    DOMcarrito.textContent = "";
    // Quitamos los duplicados
    const carritoSinDuplicados = [...new Set(carrito)];
    // Generamos los Nodos a partir de carrito
    carritoSinDuplicados.forEach((item) => {
      //Ya que existen 2 categorias registradas en la bd. la variable baseDedDatos traera [{idCat:1},{idCat:2}], por lo que debemos usar concat
      //Para formar un unico arreglo de objetos
      let arrayConcat = [];
      baseDeDatosTotal.forEach((itemBaseDatos) => {
        arrayConcat = arrayConcat.concat(itemBaseDatos);
      });

      // Obtenemos el item que necesitamos de la variable base de datos
      const miItem = arrayConcat.filter((itemBaseDatos) => {
        // ¿Coincide las id? Solo puede existir un caso
        return itemBaseDatos.id === parseInt(item);
      });
      // Cuenta el número de veces que se repite el producto
      const numeroUnidadesItem = carrito.reduce((total, itemId) => {
        // ¿Coincide las id? Incremento el contador, en caso contrario no mantengo
        return itemId === item ? (total += 1) : total;
      }, 0);
      // Creamos el nodo del item del carrito
      const miNodo = document.createElement("li");
      miNodo.classList.add("list-group-item", "text-right", "mx-2");
      miNodo.textContent = `${numeroUnidadesItem} x ${miItem[0].name} - ${miItem[0].price}€`;
      // Boton de borrar
      const miBoton = document.createElement("button");
      miBoton.classList.add("btn", "btn-danger", "mx-5");
      miBoton.textContent = "X";
      miBoton.style.marginLeft = "1rem";
      miBoton.dataset.item = item;
      miBoton.addEventListener("click", borrarItemCarrito);
      // Mezclamos nodos
      miNodo.appendChild(miBoton);
      DOMcarrito.appendChild(miNodo);
    });
  }

  /**
   * Evento para borrar un elemento del carrito
   */
  function borrarItemCarrito(evento) {
    // Obtenemos el producto ID que hay en el boton pulsado
    const id = evento.target.dataset.item;
    // Borramos todos los productos
    carrito = carrito.filter((carritoId) => {
      return carritoId !== id;
    });
    // volvemos a renderizar
    renderizarCarrito();
    // Calculamos de nuevo el precio
    calcularTotal();
  }

  /**
   * Calcula el precio total teniendo en cuenta los productos repetidos
   */
  function calcularTotal() {
    // Limpiamos precio anterior
    total = 0;
    // Recorremos el array del carrito
    //[3]
    carrito.forEach((item) => {
      // De cada elemento obtenemos su precio

      let arrayConcat = [];
      baseDeDatosTotal.forEach((itemBaseDatos) => {
        arrayConcat = arrayConcat.concat(itemBaseDatos);
      });

      const miItem = arrayConcat.filter((itemBaseDatoss) => {
        return itemBaseDatoss.id === parseInt(item);
      });
      total = total + miItem[0].price;
    });
    // Renderizamos el precio en el HTML
    DOMtotal.textContent = total.toFixed(2);
  }

  /**
   * Varia el carrito y vuelve a dibujarlo
   */
  function vaciarCarrito() {
    // Limpiamos los productos guardados
    carrito = [];
    // Renderizamos los cambios
    renderizarCarrito();
    calcularTotal();
  }

  // Eventos
  DOMbotonVaciar.addEventListener("click", vaciarCarrito);

  // Inicio
  renderizarProductos();
  input.addEventListener("keyup", async (event) => {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      event.preventDefault();
      try {
        const value = input.value;
        if (value.length <= 0) {
          const url =
            "https://bsale-backend-pe.herokuapp.com/product/search/all";
          const resp = await fetch(url);
          baseDeDatos = null;
          baseDeDatos = await resp.json();
          DOMitems.innerHTML = "";
          renderizarProductos();
          return;
        }
        const url =
          "https://bsale-backend-pe.herokuapp.com/product/search/" + value;
        const resp = await fetch(url);
        baseDeDatos = null;
        baseDeDatos = await resp.json();
        DOMitems.innerHTML = "";
        renderizarProductos();
      } catch (error) {}
    }
  });
};
